import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UtilitiesService } from 'app/utilities.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-coin-display',
  templateUrl: './coin-display.component.html',
  styleUrls: ['./coin-display.component.scss']
})

export class CoinDisplayComponent implements OnInit {

  @Input() set coin(coin: string) {
    this.symbol = coin;
    this.notExplore = false;
    this.initData();
  }

  public dateForm = this.fb.group({
    serializedDate: new FormControl(new Date(), [Validators.required])
  })
  public showGraph = false;
  public notExplore = true;
  public todayDate = new Date();
  public specificCoinData: any;
  public symbol: string;
  public subscription: Subscription;
  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
    maintainAspectRatio: false
  };
  public barChartType = 'line';
  public barChartLegend = true;
  public barChartData;
  public barChartLabels;

  constructor(
    private service: UtilitiesService,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.symbol = params.get('coinSymbol');
      this.initData();
    })
  }

  show(): void {
    this.initData();
    this.barChartData[0].label = "USD/" + this.symbol;
    const startDate = this.dateForm.get('serializedDate').value.toISOString();
    this.subscription = this.service.getSpecificCoinData(this.symbol, startDate).subscribe(specificCoinData => {
      this.specificCoinData = specificCoinData;
      this.specificCoinData.forEach(el => {
        const dateNumber = Date.parse(el.time_period_start);
        const date = new Date(dateNumber);
        const day = date.getDate();
        const month = date.getMonth() + 1;
        const year = date.getFullYear();
        const value = parseFloat(el.price_close);
        this.barChartLabels.push(day+"-"+month+"-"+year);
        this.barChartData[0].data.push(value);
      })
    });
    this.showGraph = true;
  }

  initData(): void {
    this.barChartData = [
      {data: [], label: ''}
    ]
    this.barChartLabels = [];
    this.showGraph = false;
  }
}
