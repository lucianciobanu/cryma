import { Component, OnInit } from '@angular/core';
import { UtilitiesService } from 'app/utilities.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public assetsId = ["BTC", "LTC", "ETH"];
  public todayDate = new Date();
  public startDate = new Date();
  public subscription: Subscription;
  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
    maintainAspectRatio: false
  };
  public barChartType = 'line';
  public barChartLegend = true;
  public barChartData = [
    {data: [], label: ''},
    {data: [], label: ''},
    {data: [], label: ''}
  ];
  public barChartLabels = [];
  public showGraph = false;

  constructor(
    private service: UtilitiesService
  ) { }

  ngOnInit(): void {
    this.startDate.setDate(this.todayDate.getDate() - 7);
    const startDateSerialized = this.startDate.toISOString();
    this.assetsId.forEach((assetId, i) =>
      this.getLastSevenDaysData(assetId, startDateSerialized, i)
    )
    this.showGraph = true;
  }

  getLastSevenDaysData(symbol, startDate, i) {
    this.subscription = this.service.getSpecificCoinData(symbol, startDate).subscribe(coinData => {
      this.barChartData[i].label = "USD/" + symbol;
      coinData.forEach(el => {
        const dateNumber = Date.parse(el.time_period_start);
        const date = new Date(dateNumber);
        const day = date.getDate();
        const month = date.getMonth() + 1;
        const year = date.getFullYear();
        const value = parseFloat(el.price_close);
        if (i === 0) {
          this.barChartLabels.push(day+"-"+month+"-"+year);
        }
        this.barChartData[i].data.push(value);
      });
    });
  }
}
