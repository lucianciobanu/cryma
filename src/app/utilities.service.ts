import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UtilitiesService {

  coinApiKeyLucian = '4CE14E16-F67E-4332-B37F-1575EB45BDE5';
  coinApiKeyKarim = '5FBCFB01-93CE-4F2A-863F-0D94BC4926DA';
  coinApiKeySerban = 'EDF5A1EE-2E36-4B5A-BD75-8D8201D7C6E2';
  coinApiKeyLucian2 = '63BBFAE9-EE52-474B-98A1-62573C5E5EE2';

  headerDictionary: HttpHeaders = new HttpHeaders ({
    'Accept': 'application/json',
    'X-CoinApi-Key': this.coinApiKeyLucian2
  });
  requestOptions = { headers: this.headerDictionary };
  apiUrl = 'https://rest.coinapi.io'
  assetsDataUrl = '/v1/assets/';
  historicDataUrl = '/v1/ohlcv/';
  symbolUrl = '/v1/symbols/'

  constructor(private http: HttpClient) {}

  getSpecificCoinData(coinSymbol, startDate): Observable<any> {
    const url = this.apiUrl + this.historicDataUrl + 'BITSTAMP_SPOT_' + coinSymbol + '_USD/history?period_id=1DAY&time_start='+startDate;
    return this.http.get(url, this.requestOptions);
     //to be deleted starting here once styling is finished and above return uncommented
    //  if (coinSymbol === 'BTC') {
    //   return new Observable(function subcribe(subscriber) {
    //     subscriber.next([
    //       {
    //         price_close: 32000,
    //         time_period_start: "2021-01-05T00:00:00.0000000Z"
    //       },
    //       {
    //         price_close: 34000,
    //         time_period_start: "2021-01-06T00:00:00.0000000Z"
    //       },
    //       {
    //         price_close: 36000,
    //         time_period_start: "2021-01-07T00:00:00.0000000Z"
    //       },
    //       {
    //         price_close: 38000,
    //         time_period_start: "2021-01-08T00:00:00.0000000Z"
    //       },
    //       {
    //         price_close: 38500,
    //         time_period_start: "2021-01-09T00:00:00.0000000Z"
    //       },
    //       {
    //         price_close: 39000,
    //         time_period_start: "2021-01-10T00:00:00.0000000Z"
    //       },
    //       {
    //         price_close: 40000,
    //         time_period_start: "2021-01-11T00:00:00.0000000Z"
    //       }
    //     ])
    //   })
    // } else if (coinSymbol === 'LTC') {
    //   return new Observable(function subcribe(subscriber) {
    //     subscriber.next([
    //       {
    //         price_close: 110,
    //         time_period_start: "2021-01-05T00:00:00.0000000Z"
    //       },
    //       {
    //         price_close: 120,
    //         time_period_start: "2021-01-06T00:00:00.0000000Z"
    //       },
    //       {
    //         price_close: 130,
    //         time_period_start: "2021-01-07T00:00:00.0000000Z"
    //       },
    //       {
    //         price_close: 170,
    //         time_period_start: "2021-01-08T00:00:00.0000000Z"
    //       },
    //       {
    //         price_close: 120,
    //         time_period_start: "2021-01-09T00:00:00.0000000Z"
    //       },
    //       {
    //         price_close: 180,
    //         time_period_start: "2021-01-10T00:00:00.0000000Z"
    //       },
    //       {
    //         price_close: 230,
    //         time_period_start: "2021-01-11T00:00:00.0000000Z"
    //       }
    //     ])
    //   })
    // } else if (coinSymbol === 'ETH') {
    //   return new Observable(function subcribe(subscriber) {
    //     subscriber.next([
    //       {
    //         price_close: 600,
    //         time_period_start: "2021-01-05T00:00:00.0000000Z"
    //       },
    //       {
    //         price_close: 700,
    //         time_period_start: "2021-01-06T00:00:00.0000000Z"
    //       },
    //       {
    //         price_close: 780,
    //         time_period_start: "2021-01-07T00:00:00.0000000Z"
    //       },
    //       {
    //         price_close: 910,
    //         time_period_start: "2021-01-08T00:00:00.0000000Z"
    //       },
    //       {
    //         price_close: 620,
    //         time_period_start: "2021-01-09T00:00:00.0000000Z"
    //       },
    //       {
    //         price_close: 590,
    //         time_period_start: "2021-01-10T00:00:00.0000000Z"
    //       },
    //       {
    //         price_close: 600,
    //         time_period_start: "2021-01-11T00:00:00.0000000Z"
    //       }
    //     ])
    //   })
    // }
    //to be deleted to here

  }
}
