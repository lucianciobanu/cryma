import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoinDisplayComponent } from './coin-display.component';

describe('CoinDisplayComponent', () => {
  let component: CoinDisplayComponent;
  let fixture: ComponentFixture<CoinDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoinDisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoinDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
