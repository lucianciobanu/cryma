import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  links = [
    { description: 'Home', routerLink: '' },
    { description: 'Bitcoin', routerLink: ['coin-display', 'BTC'] }, 
    { description: 'Litecoin', routerLink: ['coin-display', 'LTC'] },
    { description: 'Ethereum', routerLink: ['coin-display', 'ETH'] },
    { description: 'Explore', routerLink: ['explore', 'explore'] },
    { description: 'About', routerLink: 'about' }
  ];
  activeLink = this.links[0];
}
